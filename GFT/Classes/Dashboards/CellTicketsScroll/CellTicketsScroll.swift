//
//  CellTicketsScroll.swift
//  GFT
//
//  Created by paresh on 21/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class CellTicketsScroll: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionTicket: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionTicket.delegate = self
        collectionTicket.dataSource = self
        
        collectionTicket.register(UINib(nibName: "CellTicket", bundle: nil), forCellWithReuseIdentifier: "CellTicket")
        collectionTicket.reloadData()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellTicket", for: indexPath) as! CellTicket
        if indexPath.row == 1
        {
            cell.imgProfile.image = UIImage(named: "page2")
        }
        else if indexPath.row == 2
        {
            cell.imgProfile.image = UIImage(named: "page3")
        }
        return cell
    }
    
    
}
