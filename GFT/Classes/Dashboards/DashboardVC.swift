//
//  DashboardVC.swift
//  GFT
//
//  Created by paresh on 19/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit
//import UPCarouselFlowLayout

class DashboardVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var collectionLiveuser: UICollectionView!
    @IBOutlet weak var tblTickit: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblTickit.register(UINib(nibName: "CellTicketsScroll", bundle: nil), forCellReuseIdentifier: "CellTicketsScroll")
        
        collectionLiveuser.register(UINib(nibName: "CellUserLiveCollection", bundle: nil), forCellWithReuseIdentifier: "CellUserLiveCollection")
        
       // let layout = UPCarouselFlowLayout()
        //layout.itemSize = CGSize(width: CGFloat(65), height: CGFloat(65))
        //layout.sideItemAlpha = 0.8
        //layout.spacingMode = .fixed(spacing: 16)
        //collectionLiveuser.collectionViewLayout = layout
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   //MARK: - Tableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTicketsScroll", for: indexPath) as! CellTicketsScroll
        cell.collectionTicket.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    //MARK: - API call
    						

}
extension DashboardVC : UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellUserLiveCollection", for: indexPath) as! CellUserLiveCollection
        
        return cell
    }
}
