//
//  CellUserLiveCollection.swift
//  GFT
//
//  Created by paresh on 20/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class CellUserLiveCollection: UICollectionViewCell {

    @IBOutlet weak var imgprofile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgprofile.layer.masksToBounds = true
        self.imgprofile.layer.cornerRadius = imgprofile.frame.size.width / 2
        // Initialization code
    }

}
