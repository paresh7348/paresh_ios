//
//  SignUpVC.swift
//  GFT
//
//  Created by paresh on 12/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
   
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet  var txtConfirmPassword: ACFloatingTextfield!
    @IBOutlet  var txtPassword: ACFloatingTextfield!
    @IBOutlet  var txtEmail: ACFloatingTextfield!
    @IBOutlet  var txtUsername: ACFloatingTextfield!
    @IBOutlet weak var imgAWppLogo: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignup.layer.cornerRadius = 1
        btnSignup.layer.borderColor = UIColor.white.cgColor
        btnSignup.layer.borderWidth = 2
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnSignupClicked(_ sender: Any) {
        self.validateSignupDetails()
        
        
    }
    @IBAction func btnloginClicked(_ sender: Any) {
        _ = Global.appDelegate.navController?.popViewController(animated: true)
    }
    //MARK: - Class methods 
    func validateSignupDetails() {
        self.txtUsername.text = self.txtUsername.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtEmail.text = self.txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtPassword.text = self.txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtConfirmPassword.text = self.txtConfirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        guard (self.txtUsername.text?.characters.count)! > 0  else {
            return Singleton.sharedSingleton.showWarningAlert(withMsg: "Please enter your username")
        }
        
        
        guard Singleton.sharedSingleton.validateEmail(strEmail: txtEmail.text!)  else {
            return Singleton.sharedSingleton.showWarningAlert(withMsg: "Please enter valid email")
        }
        
        guard (self.txtPassword.text?.characters.count)! > 0  else {
            return Singleton.sharedSingleton.showWarningAlert(withMsg: "Please enter your password")
        }

        guard txtPassword.text! == txtConfirmPassword.text! else {
            return Singleton.sharedSingleton.showWarningAlert(withMsg: "Password do not match.")
        }
        self.callAPISignup()
        
    }

    //MARK : API CALL
    func callAPISignup() {
        let param = NSMutableDictionary()
        param.setValue("7046070427", forKey: "phone")
        param.setValue(txtUsername.text!, forKey: "name")
        param.setValue(txtEmail.text!, forKey: "email")
        param.setValue(txtPassword.text!, forKey: "password")
        param.setValue("i", forKey: "device_type")
        param.setValue("i564d6g564hyttr54t5t54yjiyhkytu4t", forKey: "push_token")
        
        CredentialManager.shared.APICallSignUp(Loaderview: self.view, param: param, success: { (crdential) in
            Singleton.sharedSingleton.saveLoginCredentials(values: crdential)
            _ = Global.appDelegate.navController?.popViewController(animated: true)
            
        }) { (error) in
            
            print(error)
        }
        
        
        
        
    }
    

}
