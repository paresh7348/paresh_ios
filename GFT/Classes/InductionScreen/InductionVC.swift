//
//  InductionVC.swift
//  GFT
//
//  Created by paresh on 12/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class InductionVC: UIViewController, UIScrollViewDelegate{
    
    
    let images: [UIImage] = [UIImage(named: "page1")!,UIImage(named: "page2")!,UIImage(named: "page3")!,UIImage(named: "page1")!,UIImage(named: "page2")!]
    
    var Scrollwidth:CGFloat = 0.0

    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //1
        let middle = UIScreen.main.bounds.width / 2
        
        self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        let scrollViewWidth:CGFloat = self.startButton.frame.width
        let scrollViewHeight:CGFloat = self.startButton.frame.width
        //2
        textView.textAlignment = .center
        textView.text = "Sweettutos.com is your blog of choice for Mobile tutorials"
        self.startButton.layer.cornerRadius = 4.0
        //3
        var imagexPostion: CGFloat = middle - (scrollViewWidth / 2)
        let imgOne = UIImageView(frame: CGRect(x:imagexPostion, y:100,width:scrollViewWidth, height:scrollViewHeight))
        imagexPostion += middle + 20
        imgOne.image = UIImage(named: "page1")
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth + (imagexPostion), y:100,width:scrollViewWidth, height:scrollViewHeight))
        imagexPostion += middle + 40
        imgTwo.image = UIImage(named: "page2")
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2 + (imagexPostion), y:100,width:scrollViewWidth, height:scrollViewHeight))
        imagexPostion += middle + 30
        imgThree.image = UIImage(named: "page3")
        let imgFour = UIImageView(frame: CGRect(x:scrollViewWidth*3 + (imagexPostion ), y:100,width:scrollViewWidth, height:scrollViewHeight))
        imgFour.image = UIImage(named: "page1")
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
        self.scrollView.addSubview(imgFour)
        // 4
        self.Scrollwidth = CGFloat(self.scrollView.frame.width * 4)
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 3, height:self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.contentSize = CGSize(width:self.Scrollwidth * 3, height:self.scrollView.frame.height)
    }
    @IBAction func btnStartClicked(_ sender: Any) {
        let home = homeLiveVC(nibName: "homeLiveVC", bundle: nil)
        Global.appDelegate.navController?.pushViewController(home, animated: true)
    }
    
}


private typealias ScrollView = InductionVC
extension ScrollView
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(currentPage) == 0{
            textView.text = "Sweettutos.com is your blog of choice for Mobile tutorials"
        }else if Int(currentPage) == 1{
            textView.text = "I write mobile tutorials mainly targeting iOS"
        }else if Int(currentPage) == 2{
            textView.text = "And sometimes I write games tutorials about Unity"
        }else{
            textView.text = "Keep visiting sweettutos.com for new coming tutorials, and don't forget to subscribe to be notified by email :)"
            // Show the "Let's Start" button in the last slide (with a fade in animation)
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.startButton.alpha = 1.0
            })
        }
    }
    
}
