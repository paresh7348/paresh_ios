//
//  QuotoVC.swift
//  GFT
//
//  Created by paresh on 12/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class QuotoVC: UIViewController {

    
    var viewContr:UIViewController?
    var SyncCall3Minut : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SyncCall3Minut = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(showInductionforfiveSec(timer:)), userInfo: nil, repeats: false)
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showInductionforfiveSec(timer:Timer)
    {
        print("the time is over")
        if self.viewContr != nil
        {
             Global.appDelegate.navController?.pushViewController(viewContr!, animated: true)
            
        }
        
    }
    
}
