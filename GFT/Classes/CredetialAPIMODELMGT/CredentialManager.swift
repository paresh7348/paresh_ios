//
//  CredentialManager.swift
//  GFT
//
//  Created by paresh on 11/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class CredentialManager: NSObject {
    
    static let shared = CredentialManager()
    
    func APICallLogin(Loaderview:UIView? ,param:NSMutableDictionary, success:@escaping AFAPICallerSuccess, failure:@escaping AFAPIFailured) {
        let apiStr = "login"
        AFAPICaller().callAPIUsingPOST(filePath: apiStr, params: param, enableInteraction: false, showLoader: true, viewObj: Loaderview, onSuccess: { (response, flag) in
            let dict = response as? [String: AnyObject] ?? [:]
            let msg = dict["msg"] as? String ?? ""
            if flag{
                let creadential = self.parseLoginData(dict: dict)
                success(creadential)
                
            }
            else{
                Singleton.sharedSingleton.ShowWarningdAlert(Message: msg, title: "Login")
            }
            
        }) {
           // Singleton.sharedSingleton.ShowWarningdAlert(Message: "Ops. something went wrong. please try again.", title: "Login")
            failure("")
        }
        
    }
    
    func parseLoginData(dict:[String: AnyObject]) -> Credential{
        let Creden  = Credential()
        Creden.userId = "1"
        return Creden
    }
    
    func APICallSignUp(Loaderview:UIView? ,param:NSMutableDictionary, success:@escaping AFAPICallerSuccess, failure:@escaping AFAPIFailured) {
        let apiStr = "signup"
        
        
        AFAPICaller().callAPIUsingPOST(filePath: apiStr, params: param, enableInteraction: false, showLoader: true, viewObj: Loaderview, onSuccess: { (response, flag) in
            let dict = response as? [String: AnyObject] ?? [:]
            let msg = dict["msg"] as? String ?? ""
            if flag {
                
                let creadential = self.parseLoginData(dict: dict)
                success(creadential)
                Singleton.sharedSingleton.ShowSuccessdAlert(Message: "Congratulation you have been successfully registered. Please login.", title: "Registration")
            }
            else{
                Singleton.sharedSingleton.ShowWarningdAlert(Message: msg, title: "Registration")
            }
            
            
        }) {
            Singleton.sharedSingleton.ShowWarningdAlert(Message: "Ops. something went wrong. please try again.", title: "Registration")
            failure("")
        }
        
    }
    func callapiforList(param:NSMutableDictionary,succes:String, view:UIView)
    {
        
        AFAPICaller.shared.callAPIUsingPOST(filePath: "", params: param, enableInteraction: true, showLoader: true, viewObj: view, onSuccess: { (success, flag) in
            
            if flag
            {
                
            }
            else
            {
                
            }
            
        }) { 
            
        }
        
    }
    
    

}

class Credential: NSObject {
    var first_name:String = ""
    var last_name:String = ""
    var middle_name:String = ""
    var email:String = ""
    var phone_number:String = ""
    var userId:String = ""
    
}

