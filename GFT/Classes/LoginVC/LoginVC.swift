//
//  LoginVC.swift
//  GFT
//
//  Created by paresh on 10/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import GoogleSignIn

class LoginVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
//

    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    var userFbData:[String:AnyObject] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
       assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        self.setDefaultvalues()

        // Do any additional setup after loading the view.
    }

    //MARK: - set default values
    func setDefaultvalues() {
        self.btnLogin.titleLabel?.font = UIFont(name: Global.kFont.QuicksandRegular, size: Singleton.sharedSingleton.getDeviceSpecificFontSize(14))
        self.btnSignup.titleLabel?.font = UIFont(name: Global.kFont.QuicksandRegular, size: Singleton.sharedSingleton.getDeviceSpecificFontSize(14))
        btnLogin.layer.cornerRadius = 1
        btnLogin.layer.borderColor = UIColor.white.cgColor
        btnLogin.layer.borderWidth = 2
        
        btnSignup.layer.cornerRadius = 1
        btnSignup.layer.borderColor = UIColor.white.cgColor
        btnSignup.layer.borderWidth = 2
        
    }
    //MARK: - Custom Methods
    func validateLoginDetails() {
        self.login()
    }
    
    
    
    //MARK: - iBActions
    @IBAction func btnLoginClicked(_ sender: Any) {
        let login = UserLoginVC(nibName: "UserLoginVC"
        , bundle: nil)
        Global.appDelegate.navController?.pushViewController(login, animated: true)
    }

    @IBAction func btnSiguUpClicked(_ sender: Any) {
        let login = SignUpVC(nibName: "SignUpVC"
            , bundle: nil)
        Global.appDelegate.navController?.pushViewController(login, animated: true)
        
    }
    
    @IBAction func btnFBLoginClicked(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
        
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.userFbData = result as! [String : AnyObject]
                    print(result!)
                    print(self.userFbData)
                }
            })
        }
    }
    
    @IBAction func btnTwitterLoginClicked(_ sender: Any) {
        
        Twitter.sharedInstance().logIn { session, error in
            if (session != nil)
            {
                print("signed in as \(session!.userName)");
                let client = TWTRAPIClient.withCurrentUser()
                let request = client.urlRequest(withMethod: "GET",
                                                url: "https://api.twitter.com/1.1/account/verify_credentials.json",
                                                parameters: ["include_email": "true", "skip_status": "true"],
                                                error: nil)
                client.sendTwitterRequest(request)
                { response, data, connectionError in
                    print(response)
                }
            }
            else
            {
                print("error: \(error!.localizedDescription)");
            }
        }
        
    }
    
    @IBAction func btnGoogleLoginClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            // ...
        } else {
            print("\(error.localizedDescription)")
            
        }
        
    }
    
   
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
      //  myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - API Call
    func login() {
        
    }
    
    
}
