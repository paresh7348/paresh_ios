//
//  UserLoginVC.swift
//  GFT
//
//  Created by paresh on 12/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class UserLoginVC: UIViewController {
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var txtUsername: ACFloatingTextfield!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.txtUsername.text = ""
        self.txtPassword.text = ""
    }

    // MARK: - IBActions
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        self.validateLoginDetails()
    }
    
    //MARK: - Class Methods
    
    func validateLoginDetails()
    {
       // let home = homeLiveVC(nibName: "homeLiveVC", bundle: nil)
       // Global.appDelegate.navController?.pushViewController(home, animated: true)
        txtUsername.text = txtUsername.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        guard Singleton.sharedSingleton.validateEmail(strEmail: txtUsername.text!)  else {
            return Singleton.sharedSingleton.showWarningAlert(withMsg: "Please enter valid email")
        }
        
        guard (self.txtPassword.text?.characters.count)! > 0  else {
            return Singleton.sharedSingleton.showWarningAlert(withMsg: "Please enter your password")
        }
        
        self.callLoginApi()
    }
    
    //MARK: API CALL
    func callLoginApi() {
        
        let param = NSMutableDictionary()
        param.setValue(txtUsername.text!, forKey: "phone")
        param.setValue(txtPassword.text!, forKey: "password")
        param.setValue("i", forKey: "device_type")
        param.setValue("654gbv32bhdgrsdvlkg44fg", forKey: "push_token")
        CredentialManager.shared.APICallLogin(Loaderview: self.view, param: param, success: { (credentials) in
            let induction = InductionVC(nibName: "InductionVC", bundle: nil)
            Global.appDelegate.navController?.pushViewController(induction, animated: true)
            //Save to user defaults
            
            Singleton.sharedSingleton.saveLoginCredentials(values: credentials)
        }) { (ERROR) in
            print(ERROR)
            let induction = InductionVC(nibName: "InductionVC", bundle: nil)
            Global.appDelegate.navController?.pushViewController(induction, animated: true)
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        _ = Global.appDelegate.navController?.popViewController(animated: true)
    }
    

}
