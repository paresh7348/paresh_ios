//
//  WorkoutStatusCell.swift
//  GFT
//
//  Created by paresh on 14/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit

class WorkoutStatusCell: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewBack.layer.cornerRadius = 10
        self.viewBack.layer.masksToBounds = true
        self.imgProfile.layer.cornerRadius = imgProfile.frame.size.height / 2
        self.imgProfile.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
