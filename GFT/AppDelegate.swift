 
//
//  AppDelegate.swift
//  GFT
//
//  Created by paresh on 10/09/1939 Saka.
//  Copyright © 1939 Saka Sulphur Technology. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Fabric
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController:UINavigationController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if Global.kLoggedInUserData().IsLoggedIn == "true"
        {
            
        }
        else{
            
            self.prepareNavigation()
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            
        }
        //Twitter
        Twitter.sharedInstance().start(withConsumerKey:"PGzRFmls7sv4kKk1oOKZP6VDx", consumerSecret:"aaRbRLIDSWH3HM4JrNPILTzZB5kU1JUEYhFI7rp0wnyouRmO5z")
        
       // Fabric.with([Twitter.sharedInstance()])
        //Google +
        
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func prepareNavigation() {
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        let quote = QuotoVC(nibName: "QuotoVC", bundle: nil)
        let login = LoginVC(nibName: "LoginVC", bundle: nil)
        if launchedBefore
        {
            
            navController = UINavigationController(rootViewController: login)
        }
        else{
            quote.viewContr = login
            
            
            navController = UINavigationController(rootViewController: quote)
            
        }
        navController?.isNavigationBarHidden = true
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        286981575157311
        if url.scheme == "fb286981575157311"
        {
          return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        else{
            return GIDSignIn.sharedInstance().handle(url as URL!,
                                                     sourceApplication: sourceApplication,
                                                     annotation: annotation)
            
        }
        
        
    }
    
//    func application(application: UIApplication,
//                     openURL url: NSURL, options: [String: AnyObject]) -> Bool {
//        return GIDSignIn.sharedInstance().handleURL(url as URL!,
//                                                    sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String,
//                                                    annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
//    }
    
    
    

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

