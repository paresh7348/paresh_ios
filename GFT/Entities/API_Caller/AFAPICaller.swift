//
//  AFAPICaller.swift
//  chilax
//
//  Created by Tops on 6/15/17.
//  Copyright © 2017 Tops. All rights reserved.
//

import UIKit
import SpinKit
import AFNetworking

class AFAPICaller: NSObject {
    
    typealias AFAPICallerSuccess = (_ responseData: Any, _ success: Bool) -> Void
    
    typealias AFAPIGoogleSuccess = (_ success: Bool) -> Void
    
    typealias AFAPICallerFailure = () -> Void
    typealias AFAPICallerFailedCall = (_ Error:String, _ Flag:Bool) -> Void
    static let shared = AFAPICaller()
    let afManagerSearch = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
    
     //MARK: -  Add loader in view
    func addShowLoaderInView(viewObj: UIView, boolShow: Bool, enableInteraction: Bool) -> UIView? {
        let viewSpinnerBg = UIView(frame: CGRect(x: (Global.screenWidth - 54.0) / 2.0, y: (Global.screenHeight - 54.0) / 2.0, width: 54.0, height: 54.0))
        viewSpinnerBg.backgroundColor = Global().RGB(r: 240, g: 240, b: 240, a: 0.4)
        viewSpinnerBg.layer.masksToBounds = true
        viewSpinnerBg.layer.cornerRadius = 5.0
        viewObj.addSubview(viewSpinnerBg)
        
        if boolShow {
            viewSpinnerBg.isHidden = false
        }
        else {
            viewSpinnerBg.isHidden = true
        }
        
        if !enableInteraction {
            viewObj.isUserInteractionEnabled = false
        }
        //add spinner in view
        let rtSpinKitspinner: RTSpinKitView = RTSpinKitView(style: RTSpinKitViewStyle.styleWave , color: UIColor.white)
        rtSpinKitspinner.center = CGPoint(x: (viewSpinnerBg.frame.size.width - 8.0) / 2.0, y: (viewSpinnerBg.frame.size.height - 8.0) / 2.0)
        rtSpinKitspinner.color = Global.kAppColor.Blue
        rtSpinKitspinner.startAnimating()
        viewSpinnerBg.addSubview(rtSpinKitspinner)
        return viewSpinnerBg
    }
    
    func addShowLoaderInViewSmallLoader(viewObj: UIView, boolShow: Bool, enableInteraction: Bool) -> UIView? {
        let viewSpinnerBg = UIView(frame: CGRect(x: (Global.screenWidth - 54.0) / 2.0, y: (Global.screenHeight - 54.0) / 2.0, width: 20.0, height: 20.0))
        viewSpinnerBg.backgroundColor = Global().RGB(r: 240, g: 240, b: 240, a: 4.0)
        viewSpinnerBg.layer.masksToBounds = true
        viewSpinnerBg.layer.cornerRadius = 5.0
        viewObj.addSubview(viewSpinnerBg)
        
        if boolShow {
            viewSpinnerBg.isHidden = false
        }
        else {
            viewSpinnerBg.isHidden = true
        }
        
        if !enableInteraction {
            viewObj.isUserInteractionEnabled = false
        }
        //add spinner in view
        let rtSpinKitspinner: RTSpinKitView = RTSpinKitView(style: RTSpinKitViewStyle.styleFadingCircle , color: UIColor.white)
        rtSpinKitspinner.center = CGPoint(x: (viewSpinnerBg.frame.size.width - 8.0) / 2.0, y: (viewSpinnerBg.frame.size.height - 8.0) / 2.0)
        rtSpinKitspinner.color = Global.kAppColor.Blue
        rtSpinKitspinner.startAnimating()
        viewSpinnerBg.addSubview(rtSpinKitspinner)
        return viewSpinnerBg
    }
    
    func callAPIUsingGETGoogleConnection(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPIGoogleSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = filePath;
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        
        afManager.get(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            
            onSuccess(true)
            
            //            if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
            //
            //            }
            //            else { //with error
            //                if (showLoader) {
            //                    Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
            //                }
            //                onSuccess(dictResponse, false)
            //            }
        }) { (task: URLSessionDataTask?, error: Error) in
            onFailure()
        }
    }
    
    
    // MARK: -  Hide and remove loader from view
    func hideRemoveLoaderFromView(removableView: UIView, mainView: UIView) {
        removableView.isHidden = true
        removableView.removeFromSuperview()
        mainView.isUserInteractionEnabled = true
    }
    
    // MARK: -  Call web service with GET method
    func callAPIUsingGETSearchHIP(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        var viewSpinner: UIView?
        if (showLoader) {
            viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        afManagerSearch.operationQueue.cancelAllOperations()
        print("URL = \(strPath) \n Param: \(String(describing: params))")
        let authValue = Global.kLoggedInUserData().AccessToken
        if authValue != nil || authValue != ""
        {
            afManagerSearch.requestSerializer.setValue(authValue, forHTTPHeaderField: "Authorization")
        }
        afManagerSearch.get(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            print(responseObject)
            if (showLoader) {
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj!)
            }
            if let dictResponse = responseObject as? [String : AnyObject]
            {
                onSuccess(dictResponse, true)
            }
            else
            {
                onSuccess([], true)
            }
            
            //            if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
            //
            //            }
            //            else { //with error
            //                if (showLoader) {
            //                    Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
            //                }
            //                onSuccess(dictResponse, false)
            //            }
        }) { (task: URLSessionDataTask?, error: Error) in
            
            if (showLoader) {
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj!)
            }
            print(error.localizedDescription)
            onFailure()
        }
    }
    
    
    // MARK: -  Call web service with GET method
    func callAPIUsingGET(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        var viewSpinner: UIView?
        if (showLoader) {
             viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        print("URL = \(strPath) \n Param: \(String(describing: params))")
        let authValue = Global.kLoggedInUserData().AccessToken
        if authValue != nil || authValue != ""
        {
            afManager.requestSerializer.setValue(authValue, forHTTPHeaderField: "Authorization")
        }
        afManager.get(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            print(responseObject)
            if (showLoader) {
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj!)
            }
            let dictResponse = responseObject as? [AnyObject] ?? []
            onSuccess(dictResponse, true)
            
//            if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
//                
//            }
//            else { //with error
//                if (showLoader) {
//                    Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
//                }
//                onSuccess(dictResponse, false)
//            }
        }) { (task: URLSessionDataTask?, error: Error) in
            
            if (showLoader) {
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj!)
            }
            print(error.localizedDescription)
            onFailure()
        }
    }
    
    func callAPIUsingGETForProfile(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        var viewSpinner: UIView?
        if (showLoader) {
            viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        print("URL = \(strPath) \n Param: \(String(describing: params))")
        let authValue = Global.kLoggedInUserData().AccessToken
        if authValue != nil || authValue != ""
        {
            afManager.requestSerializer.setValue(authValue, forHTTPHeaderField: "Authorization")
        }
        afManager.get(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            print(responseObject ?? "--")
            if (showLoader) {
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj!)
            }
            if let dictResponse = responseObject as? NSDictionary{
                if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
                    onSuccess(dictResponse, true)
                }
                else { //with error
                    if (showLoader) {
                        if ((dictResponse.object(forKey: "flag") as? String) != nil){
                            Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
                        }
                    }
                    onSuccess(dictResponse, false)
                }
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            
            if (showLoader) {
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj!)
            }
            print(error.localizedDescription)
            onFailure()
        }
    }
    
    func callAPIUsingPOSTAny(filePath: String, params: String?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        var viewSpinner: UIView!
        if showLoader{
            viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        let authValue = Global.kLoggedInUserData().AccessToken
        if authValue != nil || authValue != ""
        {
            afManager.requestSerializer.setValue(authValue, forHTTPHeaderField: "Authorization")
        }
        //print(params ?? "")
        afManager.post(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            if showLoader{
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
            if let dictResponse = responseObject as? NSDictionary{
                print(dictResponse)
                if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
                    onSuccess(dictResponse, true)
                }
                else { //with error
                    if (showLoader) {
                        if ((dictResponse.object(forKey: "flag") as? String) != nil){
                            Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
                        }
                    }
                    onSuccess(dictResponse, false)
                }
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            
            
            onFailure()
            if (showLoader) {
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
        }
    }
    
    
    func callAPIUsingPOST(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        
        print("URL = \(strPath) \n Param: \(String(describing: params))")
        var viewSpinner: UIView!
        if showLoader{
           viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        afManager.responseSerializer = AFHTTPResponseSerializer()
        //afManager.requestSerializer = AFJSONRequestSerializer()
        //afManager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //print(params ?? "")
        afManager.post(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            
            if showLoader{
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
            
            if let anyobj = responseObject as? Data
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: anyobj, options: .allowFragments) as! [String:AnyObject]
                    print(json)
                
                let dictResponse = json
                print(dictResponse)
                if (dictResponse["success"] as? Bool == true) { //no error
                    onSuccess(dictResponse, true)
                    
                }
                else { //with error
                    if (showLoader) {
                        self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
                        
                    }
                    onSuccess(dictResponse, false)
                }
                } catch let error as NSError {
                    print(error)
                }
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            onFailure()
            if (showLoader) {
                print(error.localizedDescription)
                //Global.singleton.ShowWarningdAlert(Message: error.localizedDescription, title: "Alert")
               // Global.singleton.showWarningAlert(withMsg: error.localizedDescription)
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
        }
    }
    
    func callAPIUsingPOSTContentType(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        if (params != nil) {
         //   params?.setValue("1", forKey: "is_device")
        }
        print("URL = \(strPath) \n Param: \(String(describing: params))")
        var viewSpinner: UIView!
        if showLoader{
            viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        afManager.requestSerializer = AFJSONRequestSerializer()
        afManager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let authValue = Global.kLoggedInUserData().AccessToken
        if authValue != nil || authValue != ""
        {
            afManager.requestSerializer.setValue(authValue, forHTTPHeaderField: "Authorization")
        }
        
        //print(params ?? "")
        afManager.post(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            
            if showLoader{
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
            let dictResponse = responseObject as! NSDictionary
            print(dictResponse)
            if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
                print(dictResponse)
                onSuccess(dictResponse, true)
                
            }
            else { //with error
                if (showLoader) {
                    if ((dictResponse.object(forKey: "flag") as? String) != nil){
                        print(dictResponse)
                        Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
                    }
                }
                onSuccess(dictResponse, false)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            onFailure()
            if (showLoader) {
                print(error.localizedDescription)
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
        }
    }
    
    // MARK: -  Call web service with one image
    func callAPIWithImage(filePath: String, params: NSMutableDictionary?, image: UIImage?, imageParamName: String, enableInteraction: Bool, showLoader: Bool, viewObj: UIView, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailedCall)) {
        let strPath = Global.baseURLPath + filePath;
        var viewSpinner: UIView?
        if showLoader{
            viewSpinner = self.addShowLoaderInView(viewObj: viewObj, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        
        afManager.post(strPath, parameters: params, constructingBodyWith: { (Data) in
            if (image != nil) {
                let imageData: Data = UIImagePNGRepresentation(image!)!
                
                Data.appendPart(withFileData: imageData, name: imageParamName, fileName: "photo.png", mimeType: "image/png")
            }
        }, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            if showLoader{
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj)
            }
            
            let dictResponse = responseObject as! NSDictionary
            if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
                onSuccess(dictResponse, true)
            }
            else { //with error
                if (showLoader) {
                    Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
                }
                onSuccess(dictResponse, false)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            
            if (showLoader) {
                self.hideRemoveLoaderFromView(removableView: viewSpinner!, mainView: viewObj)
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
            }
            onFailure(error.localizedDescription, false)
        }
    }
    
    // MARK: -  Call web service with multi image
    func callAPIWithMultiImage(filePath: String, params: NSMutableDictionary?, images: [UIImage], imageParamNames: [String], enableInteraction: Bool, showLoader: Bool, viewObj: UIView, onSuccess: @escaping (AFAPICallerSuccess), onFailure: (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        
        let viewSpinner: UIView = self.addShowLoaderInView(viewObj: viewObj, boolShow: showLoader, enableInteraction: enableInteraction)!
        
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        
        afManager.post(strPath, parameters: params, constructingBodyWith: { (Data) in
            var i: Int = 0
            for image in images {
                let imageData: Data = UIImagePNGRepresentation(image)!
                Data.appendPart(withFileData: imageData, name: imageParamNames[i], fileName: "photo.png", mimeType: "image/png")
                i = i+1;
            }
        }, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            
            self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj)
            
            let dictResponse = responseObject as! NSDictionary
            if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
                onSuccess(dictResponse, true)
            }
            else { //with error
                if (showLoader) {
                    Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
                }
                onSuccess(dictResponse, false)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
         
            self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj)
            if (showLoader) {
                Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
            }
        }
    }
    
    // MARK: -  Call web service with POST method
    func callAPIUsingPOSTRawJson(filePath: String, params: NSMutableDictionary?, enableInteraction: Bool, showLoader: Bool, viewObj: UIView?, onSuccess: @escaping (AFAPICallerSuccess), onFailure: @escaping (AFAPICallerFailure)) {
        let strPath = Global.baseURLPath + filePath;
        var viewSpinner: UIView!
        if showLoader{
            viewSpinner = self.addShowLoaderInView(viewObj: viewObj!, boolShow: showLoader, enableInteraction: enableInteraction)!
        }
        let afManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        
        afManager.requestSerializer = AFJSONRequestSerializer()
        afManager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        afManager.responseSerializer = AFHTTPResponseSerializer()
        // print(params)
        afManager.post(strPath, parameters: params, progress: nil, success: { (task: URLSessionDataTask, responseObject: Any?) in
            if showLoader{
                self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            }
            if let dictResponse = responseObject as? NSDictionary
            {
                
                
                // print(dictResponse)
                if (dictResponse.object(forKey: "flag") as! Bool == true) { //no error
                    onSuccess(dictResponse, true)
                }
                else { //with error
                    self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
                    if (showLoader) {
                        if ((dictResponse.object(forKey: "flag") as? String) != nil){
                            Global.singleton.showWarningAlert(withMsg: dictResponse.object(forKey: "response") as! String)
                        }
                    }
                    onSuccess(dictResponse, false)
                }
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            
            print(error.localizedDescription)
            onFailure()
            self.hideRemoveLoaderFromView(removableView: viewSpinner, mainView: viewObj!)
            if (showLoader) {
                if (Global.kLoggedInUserData().IsLoggedIn == "true"){
                    Global.singleton.showWarningAlert(withMsg: LocalizeHelper().localizedString(forKey: "keyInternetMsg"))
                   
                }
            }
        }
    }
    
    
}
